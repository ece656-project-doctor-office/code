<?php require_once ('pages/global.php'); ?>
<?php require_once ('pages/functions.php'); ?>
<?php require_once ('php/actions.php'); ?>
<html lang="en">
    <head>
        <?php require_once ('pages/head.php'); ?>
    </head>
    <body class="doc-off">
        <section class="section header">
            <?php require_once ('pages/header.php'); ?>
        </section>
        <section class="section menu">
            <?php require_once ('pages/menu.php'); ?>
        </section>
        <section class="section content">
            <?php require_once ('pages/content.php'); ?>
        </section>
        <section class="section footer">
            <?php require_once ('pages/footer.php'); ?>
        </section>
    </body>
</html>
