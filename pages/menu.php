<div class="contained-width">
    <ul>
        <li>
            <a href="/"<?php if ($action == "") { ?> class="active"<?php } ?>/>
                Home
            </a>
        </li>
        <?php if ($logged_in) { ?>
            <li style="float:right">
                <a href="/logout"<?php if ($action == "logout") { ?> class="active"<?php } ?>/>
                    Logout
                </a>
            </li>
        <?php } else { ?>
            <li style="float:right">
                <a href="/login"<?php if ($action == "login") { ?> class="active"<?php } ?>/>
                Login
                </a>
            </li>
        <?php } ?>
    </ul>
</div>