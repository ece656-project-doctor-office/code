<div class="header__image">
    <picture>
        <source srcset="/images/header/doc_office_xlarge.png" media="(min-width: 1201px)">
        <source srcset="/images/header/doc_office_large.png" media="(min-width: 1025px)">
        <source srcset="/images/header/doc_office_med.png" media="(min-width: 769px)">
        <source srcset="/images/header/doc_office_small.png" media="(min-width: 481px)">
        <source srcset="/images/header/doc_office_xsmall.png" media="(min-width: 320px)">
        <img src="/images/header/doc_office_large.png" alt="Doctor's Office" />
    </picture>
</div>