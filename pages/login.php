<form action="/login" method="post">
    <div class="form-group">
        <label>Username</label>
        <input type="text" name="username" class="form-control" value="">
        <span class="invalid-feedback"></span>
    </div>
    <div class="form-group">
        <label>Password</label>
        <input type="password" name="password" class="form-control">
        <span class="invalid-feedback"></span>
    </div>
    <div class="form-group">
        <input type="submit" class="btn btn-primary" value="Login">
    </div>
    <p>Don't have an account? <a href="register.php">Sign up now</a>.</p>
</form>