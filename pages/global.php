<?php
    $link = mysqli_connect("database", "root", $_ENV['MYSQL_ROOT_PASSWORD'], 'docker');

    if (!$link) {
        echo "Error: Unable to connect to MySQL." . PHP_EOL;
        echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL;
        exit;
    }

    $action = str_replace("/", "", $_SERVER['REQUEST_URI']);

    session_start();

    // Check if the user is logged in, if not then redirect him to login page
    if(isset($_SESSION["logged_in"]) && $_SESSION["logged_in"] == true){
       $logged_in = TRUE;
    }
    else {
        $logged_in = FALSE;
    }

?>