<div class="contained-width">
    <?php
        switch ($action) {
            case 'login':
                require_once ('pages/login.php');
                break;
            case 'dashboard':
                require_once ('pages/dashboard.php');
                break;
            default:
                require_once ('pages/home.php');
                break;
        }
    ?>
</div>